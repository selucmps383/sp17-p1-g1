﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace InventoryTracker.Models
{
    public class DBContexts : DbContext
    {
        public DBContexts(DbContextOptions<DBContexts> options): base(options)
        {

        }
        public DbSet<InventoryItem> InventoryItems { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //inventory stuff
            modelBuilder.Entity<InventoryItem>()
                .HasAlternateKey(i => i.ID)
                .HasName("UniqueKey_InventoryID");

            modelBuilder.Entity<InventoryItem>()
                ;
            //user stuff
            modelBuilder.Entity<User>()
                .HasAlternateKey(u => u.ID)
                .HasName("UniqueKey_UserID");

            modelBuilder.Entity<User>()
                .HasAlternateKey(u => u.Username)
                .HasName("UniqueKey_Username");

            modelBuilder.Entity<User>()
                .HasMany(u => u.InventoryItems)
                .WithOne()
                .HasForeignKey(i => i.CreatedByUserID);
        }
    }
}
