﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryTracker.Models
{
    public class User
    {
        public int ID { get; set; }
        public String Username { get; set;}
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Password { get; set; }

        public List<InventoryItem> InventoryItems { get; set; }
    }

}
