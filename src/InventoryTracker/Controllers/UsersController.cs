﻿using InventoryTracker.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InventoryTracker.Controllers
{
    public class UsersController : Controller
    {
        private DBContexts _context;

        public UsersController(DBContexts context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View(_context.Users.ToList());
        }
        [Authorize(Policy = "Dickbuttz")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "Dickbuttz")]
        public IActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {

                user.Password = CryptoHelper.Crypto.HashPassword(user.Password);
                _context.Users.Add(user);
                _context.SaveChanges();
                return RedirectToAction("Login");
            }

            return View(user);
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View("Login");

        }
        [HttpPost]
        public async Task<IActionResult> Login(String username, String password)
        {
            var hashedPassList = from u in _context.Users where u.Username.Equals(username) select u;
            if (!hashedPassList.Any())
            {
                return View("Login");

            }
            User userRequested = hashedPassList.Last();
            List<Claim> claims = new List<Claim>
            {
                new Claim("Rick","Sanchez")
            };
            var id = new ClaimsIdentity(claims,"local","name","role");
            if (CryptoHelper.Crypto.VerifyHashedPassword(userRequested.Password, password))
            {

                ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(id);
                await HttpContext.Authentication.SignInAsync("MyCookieMiddlewareInstance", claimsPrincipal);
                return View("Create");
            }
            else
            {
                return View("Login");
            }
        }
        [Authorize(Policy = "Dickbuttz")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("MyCookieMiddlewareInstance");
            return View("Login");
        }



    }
}

