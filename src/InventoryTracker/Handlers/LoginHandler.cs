﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InventoryTracker.Handlers
{
    public class LoginHandlerRequirement : IAuthorizationRequirement
    {
    }
    public class LoginHandler : AuthorizationHandler<LoginHandlerRequirement> {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, LoginHandlerRequirement requirement)
        {
            context.Succeed(requirement);
            return Task.CompletedTask;
        }
    }
}
