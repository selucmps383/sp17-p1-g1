﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using InventoryTracker.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using CryptoHelper;
using Microsoft.AspNetCore.Authorization;
using InventoryTracker.Handlers;

namespace InventoryTracker
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);
            var connection = @"Server=(localdb)\mssqllocaldb;Database=InventoryTracker;Trusted_Connection=True;";
            services.AddDbContext<DBContexts>(options => options.UseSqlServer(connection));
            services.AddAuthorization(options =>
            {
                options.AddPolicy("Dickbuttz",
                    policy => policy.RequireAssertion(context => context.User.HasClaim(c => (true))));
            });
            services.AddSingleton<IAuthorizationHandler, LoginHandler>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetService<DBContexts>();
                dbContext.Database.Migrate();

                if (!dbContext.Users.Any())
                {
                    string adminpass = CryptoHelper.Crypto.HashPassword("selu2017");
                    dbContext.Users.Add(new User
                    {
                        Username = "admin",
                        FirstName = "Admin",
                        LastName = "McAdminface",
                        Password = adminpass
                    });
                    dbContext.SaveChanges();
                }

                if (!dbContext.InventoryItems.Any())
                {
                    dbContext.InventoryItems.Add(new InventoryItem
                    {
                        CreatedByUserID = 01,
                        Name = "Pepperoni Pizza",
                        Quantity = 25
                    });

                    dbContext.InventoryItems.Add(new InventoryItem
                    {
                        CreatedByUserID = 01,
                        Name = "Sausage Pizza",
                        Quantity = 900
                    });

                    dbContext.InventoryItems.Add(new InventoryItem
                    {
                        CreatedByUserID = 01,
                        Name = "Mushroom Pizza",
                        Quantity = 8
                    });

                    dbContext.SaveChanges();
                }
            }


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseCookieAuthentication(new CookieAuthenticationOptions()
            {
                AuthenticationScheme = "MyCookieMiddlewareInstance",
                LoginPath = new PathString("/Users/Unauthorized/"),
                AccessDeniedPath = new PathString("/Users/Forbidden/"),
                AutomaticAuthenticate = true,
                AutomaticChallenge = true
            });

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=InventoryItems}/{action=Index}/{id?}");
            });
        }
    }
}
